package br.ucsal.bes20181.poo.gestaoDeLamiTUI;

import java.util.Scanner;

import br.ucsal.bes20181.poo.gestaoDeLamiBO.SalaBO;
import br.ucsal.bes20181.poo.gestaoDeLamiDOMAIN.SalaDOMAIN;

public class MainActivity {

	static SalaBO salaBO = new SalaBO();
	static SalaDOMAIN salaDOMAIN = new SalaDOMAIN(null, null, null,null);
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\nDeseja criar sala? (1)");
			System.out.println("Deseja remover sala? (2)");
			System.out.println("Deseja editar sala? (3)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				salaBO.adicionarSala(salaDOMAIN);
			} else if (escolha == 2) {
				salaBO.removerSala(salaDOMAIN);
			} else if (escolha == 3) {
				salaBO.editarSala(salaDOMAIN);
			}
			salaBO.listarSala();
		}
	}
}