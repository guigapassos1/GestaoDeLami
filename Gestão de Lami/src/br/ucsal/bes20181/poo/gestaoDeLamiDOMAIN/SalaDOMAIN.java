package br.ucsal.bes20181.poo.gestaoDeLamiDOMAIN;

import java.util.ArrayList;

public class SalaDOMAIN {

	private String nome;
	private Integer capacidadeDePessoas;
	private Integer quantidadeTotalPcs;
	private Integer pcsFuncionando;
	static ArrayList<SalaDOMAIN> salas = new ArrayList<>();

	public SalaDOMAIN(String nome, Integer capacidadeDePessoas, Integer quantidadeTotalPcs, Integer pcsFuncionando) {
		this.nome = nome;
		this.capacidadeDePessoas = capacidadeDePessoas;
		this.quantidadeTotalPcs = quantidadeTotalPcs;
		this.pcsFuncionando = pcsFuncionando;
	}

	public static ArrayList<SalaDOMAIN> getSalas() {
		return salas;
	}

	public static void setSalas(ArrayList<SalaDOMAIN> sala) {
		SalaDOMAIN.salas = sala;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCapacidadeDePessoas() {
		return capacidadeDePessoas;
	}

	public void setCapacidadeDePessoas(Integer capacidadeDePessoas) {
		this.capacidadeDePessoas = capacidadeDePessoas;
	}

	public Integer getQuantidadeTotalPcs() {
		return quantidadeTotalPcs;
	}

	public void setQuantidadeTotalPcs(Integer quantidadeTotalPcs) {
		this.quantidadeTotalPcs = quantidadeTotalPcs;
	}

	public Integer getPcsFuncionando() {
		return pcsFuncionando;
	}

	public void setPcsFuncionando(Integer pcsFuncionando) {
		this.pcsFuncionando = pcsFuncionando;
	}

}