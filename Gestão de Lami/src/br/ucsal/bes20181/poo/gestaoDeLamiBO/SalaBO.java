package br.ucsal.bes20181.poo.gestaoDeLamiBO;

import java.util.Scanner;

import br.ucsal.bes20181.poo.gestaoDeLamiDOMAIN.SalaDOMAIN;

public class SalaBO {

	Scanner sc = new Scanner(System.in);

	public void adicionarSala(SalaDOMAIN salaDOMAIN) {
		SalaDOMAIN novaSala = criarSala(salaDOMAIN);
		boolean existe = false;
		for (SalaDOMAIN lista : SalaDOMAIN.getSalas()) {
			if (salaDOMAIN.getNome().equals(lista.getNome())) {
				existe = true;
				System.out.println("\n#Nome j� usado#");
			}
		}
		if(!existe) {
			SalaDOMAIN.getSalas().add(novaSala);
		}
	}

	private SalaDOMAIN criarSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----CRIAR SALA-----");
		System.out.println("Nome da sala:");
		salaDOMAIN.setNome(sc.next());
		System.out.println("Capacidade total de pessoas:");
		salaDOMAIN.setCapacidadeDePessoas(sc.nextInt());
		System.out.println("Quantidade total de computadores:");
		salaDOMAIN.setQuantidadeTotalPcs(sc.nextInt());
		System.out.println("Quantidade de computadores funcionando:");
		salaDOMAIN.setPcsFuncionando(sc.nextInt());
		SalaDOMAIN novaSala = new SalaDOMAIN(salaDOMAIN.getNome(), salaDOMAIN.getCapacidadeDePessoas(),
				salaDOMAIN.getQuantidadeTotalPcs(), salaDOMAIN.getPcsFuncionando());
		return novaSala;
	}

	public void removerSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----REMOVER SALA-----");
		System.out.println("Digite o nome da sala que deseja remover:");
		salaDOMAIN.setNome(sc.next());
		boolean existe = false;
		for (SalaDOMAIN lista : SalaDOMAIN.getSalas()) {
			if (salaDOMAIN.getNome().equals(lista.getNome())) {
				existe = true;
				SalaDOMAIN.getSalas().remove(lista);
				System.out.println("\n#Sala removida#");
				break;
			}
		}
		if(!existe) {
			System.out.println("\n#Sala inexistente#");
		}
	}

	public void editarSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----EDITAR SALA-----");
		System.out.println("Digite o nome da sala que deseja editar:");
		salaDOMAIN.setNome(sc.next());
		boolean existe = false;
		for (SalaDOMAIN lista : SalaDOMAIN.getSalas()) {
			if (salaDOMAIN.getNome().equals(lista.getNome())) {
				existe = true;
				escolherOqueEditar(lista);
				break;
			}
		}
		if(!existe) {
			System.out.println("\n#Sala inexistente#");
		}

	}

	private void escolherOqueEditar(SalaDOMAIN lista) {
		int escolha;
		System.out.println("\nDeseja editar nome? (1)");
		System.out.println("Deseja editar capacidade de pessoas? (2)");
		System.out.println("Deseja editar quantidade total de computadores? (3)");
		System.out.println("Deseja editar quantidade de computadores funcionando? (4)\n");
		escolha = sc.nextInt();
		if (escolha == 1) {
			System.out.println("Novo nome:");
			lista.setNome(sc.next());
		} else if (escolha == 2) {
			System.out.println("Nova capacidade de pessoas:");
			lista.setCapacidadeDePessoas(sc.nextInt());
		} else if (escolha == 3) {
			System.out.println("Nova quantidade total de computadores:");
			lista.setQuantidadeTotalPcs(sc.nextInt());
		}
		else if (escolha == 4) {
			System.out.println("Nova quantidade de computadores funcionando:");
			lista.setPcsFuncionando(sc.nextInt());
		}
	}

	public void listarSala() {
		System.out.println("\n-----LISTA DE SALAS-----\n");
		for (SalaDOMAIN lista : SalaDOMAIN.getSalas()) {
			System.out.println("Sala: " + lista.getNome());
			System.out.println("Capacidade total de pessoas: " + lista.getCapacidadeDePessoas());
			System.out.println("Quantidade total de computadores: " + lista.getQuantidadeTotalPcs());
			System.out.println("Quantidade de computadores funcionando: " + lista.getPcsFuncionando());
			System.out.println("------------------------\n");
		}
	}

}